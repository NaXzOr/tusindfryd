namespace TusindFryd;

public class BlomsterSort
{
    public static BlomsterSort Kornblomst = new("Kornblomst", 150, 30, 0.01);

    public static BlomsterSort Marguritter = new("Marguritter", 11, 10, 0.1);

    public static BlomsterSort BourbonQueen = new("Bourbon Queen", 75, 50, 0.4);
    public BlomsterSort(string navn, int halveringsTid, int produktionsTid, double størrelseM2)
    {
        Navn = navn;
        HalveringsTid = halveringsTid;
        ProduktionsTid = produktionsTid;
        StørrelseM2 = størrelseM2;
    }

    public string Navn { get; }
    public int HalveringsTid { get; }
    public int ProduktionsTid { get; }
    public double StørrelseM2 { get; }
}