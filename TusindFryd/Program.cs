﻿namespace TusindFryd;

public class Program
{
    public static void Main(string[] args)
    {
        var Drivhus1 = new Drivhus()
        {
            DrivhusNavn = "Drivhus1",
            ProduktionsBakkes = new List<ProduktionsBakke>()
            {
                new ProduktionsBakke()
                {
                    BlomsterSort = BlomsterSort.Kornblomst,
                    StartAntal = 1000,
                    ForventetSlutAntal = 871,
                    OptaltDato = DateTime.Parse("10/04-2015"),
                    ProduktionsDage = 30,
                    OptaltAf = "Hr",
                    OptaltAntal = 875,
                    StørrelseIM2 = 10,
                    Afsluttet = DateTime.Now.AddDays(-1)
                },
                new ProduktionsBakke()
                {
                    BlomsterSort = BlomsterSort.Marguritter,

                }
            }
        };
        var Drivhus2 = new Drivhus() { DrivhusNavn = "Drivhus2" };
        var Drivhus3 = new Drivhus() { DrivhusNavn = "Drivhus3" };
        var Drivhus4 = new Drivhus() { DrivhusNavn = "Drivhus4" };
    }
}