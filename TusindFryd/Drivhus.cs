namespace TusindFryd;

public class Drivhus
{
    public string DrivhusNavn { get; set; }
    public List<ProduktionsBakke> ProduktionsBakkes {get; set; }
}