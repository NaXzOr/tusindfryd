namespace TusindFryd;

public class ProduktionsBakke
{
    public int OptaltAntal { get; set; }
    public DateTime OptaltDato { get; set; }
    public DateTime? Afsluttet{ get; set; }
    public int ProduktionsDage { get; set; }
    public string OptaltAf { get; set; }
    public int StartAntal { get; set; }
    public int ForventetSlutAntal { get; set; }
    public double StørrelseIM2 { get; set; }
    public BlomsterSort BlomsterSort { get; set; }

    public string AfvigelseIProcent => $"{Math.Round(((double)OptaltAntal / ForventetSlutAntal-1)*100, 2)}%";

    public bool ErAfsluttet => Afsluttet.HasValue;
}